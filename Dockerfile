FROM debian:stable-slim
MAINTAINER gnorth <https://hub.docker.com/u/gnorth/>
RUN apt --quiet update --yes
RUN apt --quiet install --yes subversion g++ zlib1g-dev build-essential git python rsync man-db libncurses5-dev gawk gettext unzip file libssl-dev wget zip time
RUN git clone https://gitlab.com/GriefNorth/luci-app-unlocker.git
RUN make -C luci-app-unlocker/translation/po2lmo
RUN make install -C luci-app-unlocker/translation/po2lmo
RUN rm -rf luci-app-unlocker
RUN wget -qO- https://downloads.openwrt.org/releases/19.07.2/targets/ar71xx/generic/openwrt-sdk-19.07.2-ar71xx-generic_gcc-7.5.0_musl.Linux-x86_64.tar.xz | tar xJf -
RUN mv openwrt* openwrtsdk